from typing import Union
from abc import abstractmethod
import gym

import numpy as np
from gym import Env, spaces
from gym.spaces import Box, Space, Discrete, Dict, MultiDiscrete
from wtpy.WtBtEngine import WtBtEngine, EngineType
from wtpy.StrategyDefs import BaseCtaStrategy, CtaContext
from wtpy.ExtModuleDefs import BaseExtDataLoader
from components import ActionPlan, RewardPlan, Observer
from default_components import *


class WtCtaEnv(gym.Env):
    def __init__(self, Strategy, observer: Observer, reward_plan: RewardPlan,
                 extloader: BaseExtDataLoader, slippage: int = 0):
        """"""
        # internal state
        self._iter_ = 0
        self._run_ = False
        self._engine_type = Strategy.engine_type()

        # components
        self._engine = None
        self._Strategy = Strategy  # 运行的env将在Strategy初始化时被赋予
        self._observer = observer
        self._observer._env = self
        self._reward_plan = reward_plan
        self._reward_plan._env = self

        self._slippage = slippage

        self.observation_space = observer.observation_space
        self.action_space = self._Strategy.action_space

        self.extloader = extloader

    @property
    def components(self) -> dict[str, Union[ActionPlan, RewardPlan, Observer]]:
        return {
            "strategy": self._strategy,
            "reward": self._reward_plan,
            "observer": self._observer
        }

    def reset(self):
        """"""
        self.close()
        self._iter_ += 1

        if not hasattr(self, '_engine'):
            self._engine = WtBtEngine(eType=self._engine_type, logCfg='./common/logcfgbt.yaml')
            self._engine.set_extended_data_loader(loader=self.extloader, bAutoTrans=True)

            if self._engine_type == EngineType.ET_CTA:
                self._engine.init('./common', 'configbt_cta.yaml')
                self._related_engine_step = self._engine.cta_step
            elif self._engine_type == EngineType.ET_HFT:
                self._engine.init('./common', 'configbt_hft.yaml')
                self._related_engine_step = self._engine.hft_step
            else:
                raise AttributeError

            self._engine.commitBTConfig()

        # 重置奖励
        self._reward_plan.reset()
        # 初始化策略加入环境
        self._strategy = self._Strategy(observer=self._observer, reward_plan=self._reward_plan)
        self._strategy._env = self

        # 安装钩子
        if self._engine_type == EngineType.ET_CTA:
            self._engine.set_cta_strategy(
                self._strategy, slippage=self._slippage, hook=True)
        elif self._engine_type == EngineType.ET_HFT:
            self._engine.set_hft_strategy(self._strategy, hook=True)
        else:
            raise AttributeError

        # 回测以异步方式运行
        self._engine.run_backtest(bAsync=True)
        self._run_ = True

        self._step()

        return self._observer.obs

    def _step(self):
        """在回测引擎推进真实一步(related_engine_step)后的收尾工作"""
        pass

    def step(self, action):
        """"""
        self._strategy.set_action(action=action)
        self._related_engine_step()
        self._step()
        return self._observer.obs, self._reward_plan.reward, self._reward_plan.done, {}

    def close(self):
        """关闭此环境"""
        self._iter_ = 0


if __name__ == '__main__':
    feature_df = pd.read_csv('storage\\csv\\CZCE.SR.HOT_d.csv')[['f1', 'f2', 'f3']]
    env = WtCtaEnv(
        Strategy=DefaultCtaStrategy,
        observer=DefaultObserver(df=feature_df),
        reward_plan=DefaultRewardPlan(),
        extloader=MyDataLoader_d()
    )
    print(env)
