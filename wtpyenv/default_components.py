import numpy as np
import pandas as pd
from wtpy import WTSBarStruct
from gym.spaces import Discrete, Box
from components import RewardPlan, Observer, ActionPlan
from wtpy.WtBtEngine import WtBtEngine, EngineType
from wtpy.StrategyDefs import BaseCtaStrategy, CtaContext
from wtpy.ExtModuleDefs import BaseExtDataLoader


class DefaultRewardPlan(RewardPlan):
    """"""

    def __init__(self):
        self._reward = []
        self._dones = []

    def calculate_reward(self, context: CtaContext):
        """"""
        # 动态权益
        dynbalance = context.stra_get_fund_data(0)
        # 总平仓盈亏
        # closeprofit = context.stra_get_fund_data(1)
        # 总浮动盈亏
        # positionprofit = context.stra_get_fund_data(2)
        # 总手续费
        # fee = context.stra_get_fund_data(3)
        self._reward.append(dynbalance)

    def calculate_done(self, context: CtaContext):
        self._dones.append(False)

    def reward(self):
        return self._reward[-1]

    def done(self):
        return self._dones[-1]


class DefaultObserver(Observer):
    """"""

    def __init__(self, df):
        """"""
        self.df_array = df.values

    @property
    def shape(self):
        return self.df_array.shape

    def observe(self):
        """"""
        self.out_obs = self.df_array[self._env._iter_]

    def obs(self):
        """"""
        return self.out_obs

    def observation_space(self):
        """"""
        return Box(**dict(low=-np.inf, high=np.inf, shape=self.shape, dtype=np.float64))


class DefaultCtaStrategy(BaseCtaStrategy, ActionPlan):
    """"""

    @staticmethod
    def engine_type() -> EngineType:
        """本策略为CTA策略"""
        return EngineType.ET_CTA

    @property
    def action_space(self):
        return Discrete(3)

    def set_action(self, action):
        """"""
        self.action = action

    def on_init(self, context: CtaContext):
        """"""

    def on_calculate(self, context: CtaContext):
        """"""
        self._env._observer.observe()
        self._env._reward_plan.calculate_reward(context)

    def on_calculate_done(self, context: CtaContext):
        """定义动作空间的值如何映射到买卖行为上"""
        print("拿到传递来的动作", self.action)
        if self.action == 0:
            pass
        elif self.action == 0:
            pass
        elif self.action == 2:
            pass
        else:
            pass


class MyDataLoader_d(BaseExtDataLoader):
    def load_final_his_bars(self, stdCode: str, period: str, feeder) -> bool:
        '''
        加载历史K线（回测、实盘）
        @stdCode    合约代码，格式如CFFEX.IF.2106
        @period     周期，m1/m5/d1
        @feeder     回调函数，feed_raw_bars(bars:POINTER(WTSBarStruct), count:int, factor:double)
        '''
        print("loading %s bars of %s from extended loader" % (period, stdCode))

        df = pd.read_csv('/storage/csv/CZCE.SR.HOT_d.csv')
        df['date'] = df.apply(lambda x: x['Unnamed: 0'][0:10], axis=1)
        df['date'] = df['date'].str.replace('-', '/')
        df['time'] = df.apply(lambda x: x['Unnamed: 0'][-8:], axis=1)

        df = df.rename(columns={
            'date': 'date',
            'time': 'time',
            'open': 'open',
            'high': 'high',
            'low': 'low',
            'close': 'close',
            'volume': 'vol',
        })
        df['date'] = df['date'].astype('datetime64').dt.strftime('%Y%m%d').astype('int64')
        df['time'] = (df['date'] - 19900000) * 10000 + df['time'].str.replace(':', '').str[:-2].astype('int')

        BUFFER = WTSBarStruct * len(df)
        buffer = BUFFER()

        def assign(procession, buffer):
            tuple(map(lambda x: setattr(buffer[x[0]], procession.name, x[1]), enumerate(procession)))

        df.apply(assign, buffer=buffer)
        print(df)
        print(buffer[0].to_dict)
        print(buffer[-1].to_dict)

        feeder(buffer, len(df))
        return True
