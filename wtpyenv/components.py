from abc import abstractmethod
from gym.spaces import Space
from wtpy.StrategyDefs import BaseCtaStrategy, CtaContext
from wtpy.WtBtEngine import EngineType
import numpy as np


class ActionPlan:

    @staticmethod
    @abstractmethod
    def engine_type() -> EngineType:
        """指定策略运行在CTA引擎还是HFT引擎"""
        raise NotImplementedError

    @property
    def action_space(self) -> Space:
        """策略可做出的动作空间"""
        raise NotImplementedError

    @abstractmethod
    def set_action(self, action):
        """保存动作到策略，在on calc done内调用动作"""
        raise NotImplementedError


class Observer:

    # @abstractmethod
    # def reset(self):
    #     """环境reset将触发各组件reset"""

    @property
    @abstractmethod
    def observation_space(self) -> Space:
        raise NotImplementedError

    @abstractmethod
    def observe(self):
        """返回该step观测"""
        raise NotImplementedError

    @property
    @abstractmethod
    def obs(self):
        """返回该step观测"""
        raise NotImplementedError


class RewardPlan:

    @abstractmethod
    def reset(self):
        """初始化奖励类内部状态"""
        raise NotImplementedError

    @abstractmethod
    def calculate_reward(self, context: CtaContext):
        """"""

    @abstractmethod
    def calculate_done(self, context: CtaContext):
        """"""

    @property
    @abstractmethod
    def reward(self):
        """"""

    @property
    @abstractmethod
    def done(self):
        """"""
