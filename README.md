# wt4elegantrl-doc


# (慢慢更新ing... 作为学习记录)


## 介绍
本项目为wt4elegantrl非官方文档。wt4elegantrl是基于wtpy开发的gym格式强化学习环境，用于支持交易场景下使用强化学习算法进行训练。 \
地址:[https://github.com/drlgistics/Wt4ElegantRL](https://github.com/drlgistics/Wt4ElegantRL)

__使用方法:__

## 项目文件
`envs.py`: \
`envs_simple_cta.py`:\
`features.py`:\
`strategies.py`:\
`assessments.py`:定义了奖励方案基类Assessment，以及一个简单的可以使用的奖励方案SimpleAssessment。\
`stoppers.py`:定义了止盈止损方案，待开发。\
`reprocess.py`:\
`analysts.py`:绩效分析，调用了wtpy中的分析组件WtBtAnalyst。\

`run_toy.py`:\
`runner.py`:\

`compare_elegantrl.py`:使用elegantrl进行训练的脚本。\
`compare_rllib.py`:使用ray-rllib进行训练的脚本。\
`compare_sb3.py`:使用stable baselines3进行训练的脚本。\
`dataset_from_storage.py`:在dsb自定义数据格式和python数据结构之间相互转化的脚本。\


## 组件

### class WtEnv
封装了WtBtEngine
* 类属性
    >`TRAINER`:1  
    >`EVALUATOR`:2  
    >`DEBUGGER`:3  
    *(在类实例中用参数mode控制环境运行在何种模式下)* 
* 属性
    >`_id_`:*(privte)* 该环境的编号 \
    >`_iter_`:*(private)* 内部记录环境进行到第几步 \
    >`_run_`:*(private)* 该环境是否在运行 \
    >`__strategy__`:*(private)* \
    >`_et_`:*(private)* \
    >`__stopper__`:*(private)* \
    >`__slippage__`:*(private)* 设置滑点\
    >`__feature__`:*(private)* 关联的Feature组件\
    >`__assessment__`:*(private)* 关联的奖励方案组件\
    >`__time_range__`:*(private)* 时间范围\
    >`__cb_step__`:*(private)* 根据策略引擎类型是CTA还是HFT,`__cb_step__` 被赋值为WtBtEngine的cta_step方法或hft_step方法。\

    >`observation_space`:*(public)* 环境状态空间,\
    >`action_space`:*(public)* 环境动作空间,\

    >`assets`:*(@property)* 返回该时间步资产数额\


* 方法
    >`def __init__(self,
                 strategy: StateTransfer,
                 stopper: Stopper,
                 feature: Feature,
                 assessment: Assessment,
                 time_range: tuple,
                 slippage: int = 0,
                 id: int = getpid(),
                 mode=1,
                 ):`
    stopper接收自定义的Stopper组件，用来控制止盈止损逻辑。
    feature接收Feature组件，该组件为特征工程逻辑，用来提供环境执行step后返回的观测observations。
    assessment:自定义奖励方案，assessment的calculate()将输出奖励值(为一个标量)\
    >`def __step__(self):`
    用于把环境向后推进一步。\
    >`def close(self):` 如果实例的_run_标志为True,并且具有_engine_,则结束引擎的回测过程并重置_run_为False\
    >`def reset(self):`重置环境，并输出初始时的observations。\
    >`def step(self, action):`智能体选择一个动作，并推进一步，返回新的观测，奖励值，是否结束标志和调试信息。\
    >`def analyst(self, iter: int):`\
    >`def analysts(self):`\
    >`def _name_(self, iter):`\

### class SimpleCTAEnv(WtEnv)
XXX
* 属性

### class Feature
因子基类，自定义的特征工程类应继承此类，并实现相关因子的计算方法或者引入外部因子数据。
* 属性
    >`securities`:(@property)\
    >`observation`:(@property) *dict* \
* 方法
    >`def __init__(self, code: str, period: str, roll: int, assets: float = 1000000) -> None:`

### class Indicator(Feature)
内置的特征工程类，提供了kdj,rsi等传统技术指标因子(内部使用talib计算)，并支持预先计算好的的因子的载入(参考weights方法中example()的伪代码)
* 方法
    >`def obv(self, period: str, reprocess: REPROCESS = MAXMIN):`在状态空间中加入obv因子。其他需要计算的因子仿照该方法写即可。\
    >`def weights(self, period: str, timeperiod:int=1, index:str='000300', reprocess:REPROCESS =REPROCESS):`
    引入外部因子数据。具体做法是:预先计算好的因子(如已经有一个因子dataframe)可通过下面的example()进行加载，example函数中写获取因子的逻辑并返回ndarray格式的特征序列构成的元组，同时指定下面的space参数为特征的数量。


### class StateTransfer
XXX
* 属性
* 方法
    >`def EngineType() -> int:`定义继承此类的策略，属于CTA还是HFT，
  之后就可以告诉WtBtEngine以何种eType去初始化 \
    `def Action(size: int) -> dict:`返回动作空间 \
    `def setAction(self, action):`1.setAction这里将action保存到子类(策略类对象)的属性中，
  为了策略类的on_calc_done方法可以方便从属性中拿到动作，从而实现动作到目标仓位的映射。
  (因为on_calc_done方法的签名无法将action带入，所以需要从setAction饶一道) 
  2.实际使用时，StateTransfer的setAction()也将动作和买卖行为相隔离。

### class SimpleCTA(BaseCtaStrategy, StateTransfer)
XXX
* 属性

### class REPROCESS
XXX
* 属性

## wtpy相关
### 一、使用自定义的数据源进行回测
这一块不难，但自己也看了好多天才成功，在这里总结一下使用wtpy回测自定义数据。在wt4erl下大家会最终是要用自己的特征数据去训练agent，这篇文档介绍如何通过wtpy加载自己的数据。
#### 1.下载demos
使用的版本为wtpy0.9，因为还没正式发布，所以去github/wtpy项目dev分支下载安装一下，并下载0.9 demos。demos里的storage文件夹内就是回测会使用的数据文件，在下一步我们会将自己的csv文件也存放在这个目录下。cta_fut_bt为期货cta回测示例。
#### 2.准备自定义数据文件
我的csv文件格式如下：
```
datetime,open,close,high,low,volume,money
2019-12-02 09:01:00,5568,5565,5570,5565,3110,173040400
2019-12-02 09:02:00,5565,5564,5566,5562,1894,105382160
2019-12-02 09:03:00,5564,5562,5565,5561,1790,99595600
2019-12-02 09:04:00,5563,5563,5564,5562,1000,55640000
2019-12-02 09:05:00,5563,5564,5564,5561,1096,60981440
2019-12-02 09:06:00,5564,5571,5573,5564,3398,189064720
```
a)可以看到数据的格式跟/storage下原有的格式不同，包括列名不同和datetime列也不同(我的文件date/time作为一列，而示例的csv中为两列)。因为这里的差异所以后面我继承BaseExtDataLoader并实现一个自己的load_final_his_bars方法用来加载我这种格式的csv。\
b)文件名要统一命名为标准格式，比如我的csv是SR的一分钟数据，那么我命名为CZCE.SR.HOT_m1.csv \
c)将准备好的数据文件放入storage/csv下 
#### 3.定义自己的DataLoader
先打开test_dataexts/testDtLoader.py，这是个用extloader加载自定义数据的例子，我们将在这上面修改：\
a)将engine.configBTStorage这句的参数改为(mode="csv", path="../")，这里的path改为的当前目录，否则原来程序会去storage/csv下读文件，而我们的文件之前说过里面的格式和标准的是不一样的，所以会读取加载失败，我们要让框架通过extloader加载自定义的数据。\
b)继承BaseExtDataLoader并实现自己的load_final_his_bars方法（0.9之前版本此方法为load_his_bars）。我们直接在原有代码的基础上修改，主要是加载csv文件和转换格式的过程，如下：
```
class MyDataLoader(BaseExtDataLoader):
    def load_final_his_bars(self, stdCode: str, period: str, feeder) -> bool:
        '''
        加载历史K线（回测、实盘）
        @stdCode    合约代码，格式如CFFEX.IF.2106
        @period     周期，m1/m5/d1
        @feeder     回调函数，feed_raw_bars(bars:POINTER(WTSBarStruct), count:int, factor:double)
        '''
        print("loading %s bars of %s from extended loader" % (period, stdCode))
        # 首先读入自己的数据文件
        # df = pd.read_csv('./storage/csv/CFFEX.IF.HOT_m1.csv')
        df = pd.read_csv('../storage/csv/CZCE.SR.HOT_m1.csv')

        # 下面三句转换自定义数据中的datetime字段到标准的date和time两个字段，并替换年月日中的-为/
        df['date'] = df.apply(lambda x: x['datetime'][0:10], axis=1)
        df['date'] = df['date'].str.replace('-', '/')
        df['time'] = df.apply(lambda x: x['datetime'][-8:], axis=1)
        # 这里转换列名
        df = df.rename(columns={
            'date': 'date',
            'time': 'time',
            'open': 'open',
            'high': 'high',
            'low': 'low',
            'close': 'close',
            'volume': 'vol',
        })
```
\
c)最后，要让回测引擎通过extloader加载数据，要在引擎初始化后加上一句：
```
engine = WtBtEngine(EngineType.ET_CTA)
engine.set_extended_data_loader(loader=MyDataLoader(), bAutoTrans=True)
```
这样engine在当前目录找不到这个csv时，会自动调用MyDataLoader.load_final_his_bars()去加载数据。run运行，会发现成功执行回测。

### 二、wtpy跑simnow仿真
跑simnow仿真和wt4erl项目并无直接关系，这里作为记录文档。\
\
首先需要更新common/hots.json,可以用test_hotpicker/testHots.py脚本自动更新,
testHots.py中函数的sdate,edate自行设置一下，例如设为近三天这样一个范围。
其次要打开数据服务datakit_fut/runDT.py,最后运行策略文件（cta_fut/run.py）。\
\
__自己写的一个订阅了多品种行情的策略，没有下单过程，只是体会一下on_xxx方法的触发__。\
\
值得注意的是onbar在每个品种BAR闭合时都触发一次，oncalculate在所有BAR都闭合后只触发一次。
```
# -- coding: utf-8 --
from typing import List
from wtpy import WtEngine, EngineType
from Strategies.DualThrust import StraDualThrust
from ConsoleIdxWriter import ConsoleIdxWriter
from wtpy import BaseCtaStrategy
from wtpy import CtaContext
import numpy as np

class MultiSymbolStrategy(BaseCtaStrategy):
    def __init__(self, name: str, codes: List[str], period: str, barcnt=50):
        BaseCtaStrategy.__init__(self, name)
        self.codes = codes
        self.period = period
        self.barcnt = barcnt

    def on_init(self, context: CtaContext):
        for code in self.codes:
            # 订阅多品种行情，并将传入品种列表的第一个作为主K
            context.stra_get_bars(code, self.period, self.barcnt, isMain=True if code == self.codes[0] else False)
        context.stra_log_text("进入 oninit")

    def on_calculate(self, context: CtaContext):
        context.stra_log_text("进入 on calculate (所有标的闭合时触发oncalculate)")

    def on_calculate_done(self, context: CtaContext):
        context.stra_log_text("进入 on calculate done (必须在回测模式下开启异步才能触发calc done)")

    def on_bar(self, context: CtaContext, stdCode: str, period: str, newBar: dict):
        context.stra_log_text("进入 on bar (单个标的闭合时触发onbar)")
        print(stdCode, period, newBar)

    def on_tick(self, context: CtaContext, stdCode: str, newTick: dict):
        context.stra_log_text("进入 on tick")
        print("时间戳%s 当前品种%s 价格%s " % (newTick[0], newTick[2], context.stra_get_price(stdCode)))

if __name__ == "__main__":
    # 创建一个运行环境，并加入策略
    env = WtEngine(EngineType.ET_CTA)
    env.init('../common/', "config.yaml")
    straInfo = MultiSymbolStrategy(name='demo1', codes=['SHFE.rb.HOT', 'SHFE.cu.HOT'], period='m1')
    env.add_cta_strategy(straInfo)
    idxWriter = ConsoleIdxWriter()
    env.set_writer(idxWriter)
    env.run()
    kw = input('press any key to exit\n')
```

### 三、csv和dsb互相转换
利用了WtDataHelper类实现csv,dsb格式的互转，这里以bar数据为例。
WtDataHelper类里的trans_bars/trans_ticks已弃用，在0.9中使用store_bars/store_ticks转化csv到dsb,
使用dump_bars/dump_ticks将某目录内的所有dsb还原成csv。\
\
我的csv读成pd.DataFrame后表结构如下(从别的数据源如数据库文件读成df后也按照这个例子转为dsb)：
```
Unnamed: 0,open,close,high,low,volume,money
2019-12-02 09:01:00,5568,5565,5570,5565,3110,173040400
2019-12-02 09:02:00,5565,5564,5566,5562,1894,105382160
```
使用如下脚本将上述格式的df转化为dsb：

```
import sqlite3
from wtpy.wrapper import WtDataHelper
from wtpy.WtCoreDefs import WTSBarStruct
import pandas as pd

def strToDate(strDate: str) -> int:
    items = strDate.split("/")
    if len(items) == 1:
        items = strDate.split("-")

    if len(items) > 1:
        return int(items[0]) * 10000 + int(items[1]) * 100 + int(items[2])
    else:
        return int(strDate)

def sqlite2df(db_addr, table_name):
    conn = sqlite3.connect(db_addr)
    return pd.read_sql("select * from %s" % (table_name), conn)


if __name__ == '__main__':
    # 我这里使用sqlite文件读成df的，也可直接读一个csv文件进来成为df
    # 这里的df格式如上一个代码块所示
    df = sqlite2df('C:\\Users\\Y\\Documents\\code\\superduo\\data\\srif.db', 'ifd')
    # 调整格式
    # 分离出date
    df['date'] = df.apply(lambda x: x['Unnamed: 0'][0:10], axis=1)
    df['date'] = df['date'].str.replace('-', '/')
    # 分离出time
    df['time'] = df.apply(lambda x: x['Unnamed: 0'][-8:], axis=1)
    df['vol'] = df['volume']

    # 转化date列从2015-01-05格式转为20150105
    df['date'] = df.apply(lambda x: strToDate(x['date']), axis=1)

    # 这里给了一个长度len_df, 告诉之后的buffer有多少个WTSBarStruct
    len_df = len(df)
    helper = WtDataHelper()

    # 这种写法是预分配一个list的长度，这样直接用下标访问不会报越界的错误，如果不预分配就会报越界的错误
    # WTSBarStruct是一个内存块，BUFFER是声明一个内存段，这个内存段连续存储了很多BAR数据，python生成这个内存段直接扔给C++处理
    BUFFER = WTSBarStruct * len_df
    buffer = BUFFER()
    i = 0
    for _, row in df.iterrows():
        curbar = buffer[i]
        curbar.date = row["date"] 
        curbar.high = float(row["high"])
        curbar.low = float(row["low"])
        curbar.open = float(row["open"])
        curbar.close = float(row["close"])
        curbar.vol = int(row["vol"])
        # hold和diff 就是现有仓单和仓单变化 ，如果用不上就都放0
        curbar.hold = 0  # 原df没有该字段的值，赋值0
        curbar.diff = 0  # 原df没有该字段的值，赋值0
        i += 1
    # buffer是整个内存段，这里通过传参告诉底层这个buffer在哪，然后有多少长度，底层会自动处理。
    helper.store_bars('test2.dsb', buffer, len_df, 'd')
    # 将本文件夹内的dsb还原为csv,文件名不变
    helper.dump_bars(".", ".")
```
最后helper.store_bars('test2.dsb', buffer, len_df, 'd')这句生成了dsb文件。\
\
可以看到脚本最后一行是用了dump_bars方法生成了test2.csv，
打开test2.csv可以看到还原为csv后数据是对的上的(还原为csv后date列都变为19900000,此问题等待wtpy更新)。虽然列的值对的上，但dsb的列名变成了:
date,time,open,high,low,close,settle,volume,turnover,open_interest,diff_interest。\
\
这是因为dsb内数据列只固定了顺序，dsb就是一个C内存块，里面只有值，按顺序放(dsb是zst压缩，dmb是不压缩)。
它的好处是dsb打开就是一块内存，读写效率高，缺点就是里面没有字段信息。数据列完全依赖顺序，
你看到的open/close，都是C定义的顺序映射，没保存在内存中。(如果想深入csv dsb这块技术的话，
可以搜一下代码更新的历史，里面很多很多python和c++的技巧)







